const functions=require('./index');
const context=require('../testing/context');
const {test} =require('@jest/globals');

test('HttpTrigger', async() =>{
    const request ={
        query: {name: 'lDiaz'}
    };

    var iteration = 1000000;
    console.time('FUNCTION 1')
    for(var i=0 ; i< iteration;i++)
    {
        await functions(context,request);
    }
    console.timeEnd('FUNCTION 1')
    expect(context.res.body).toContain('H');
    expect(context.res.body).toEqual('Hello, lDiaz');
    // expect(context.log.mock.calls.length).toBe(100);
});
